from django.shortcuts import render

# Generic View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Resolving URLs
from django.urls import reverse_lazy

# Models
from .models import Region, Township

# Forms
from .forms import RegionForm, TownshipForm

def index(request):
    return render(request,'country/index.html')

# List Region
class RegionList(ListView):
    model = Region

# Detail view
class RegionView(DetailView):
    model = Region

# Create view
class RegionCreate(CreateView):
    model = Region
    form_class = RegionForm
    success_url = reverse_lazy('region_list')

# Update View
class RegionUpdate(UpdateView):
    model = Region
    form_class = RegionForm
    # Setting returning URL
    success_url = reverse_lazy('region_list')

# Delete View
class RegionDelete(DeleteView):
    model = Region
    # Setting returning URL
    success_url = reverse_lazy('region_list')    



class TownshipList(ListView):
    model = Township


# Detail view
class TownshipView(DetailView):
    model = Township

# Create view
class TownshipCreate(CreateView):
    model = Township
    form_class = TownshipForm
    success_url = reverse_lazy('township_list')

# Update View
class TownshipUpdate(UpdateView):
    model = Township
    form_class = TownshipForm
    # Setting returning URL
    def form_valid(self, form):
        township = Township.objects.get(id=form.instance.pk)       
        form.instance.name = 'hola'
        form.instance.regions.set([]) 
        self.object = form.save()
        return super(TownshipUpdate, self).form_valid(form)

    success_url = reverse_lazy('township_list')
    

# Delete View
class TownshipDelete(DeleteView):
    model = Township
    # Setting returning URL
    success_url = reverse_lazy('township_list')        