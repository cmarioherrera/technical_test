from django.contrib import admin
from country.models import Township, Region
# Register your models here.

admin.site.register(Township)
admin.site.register(Region)