from django.db import models

# Create your models here.


class Region(models.Model):
    """Model region"""
    code = models.IntegerField()
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Township(models.Model):
    """Model township"""
    code = models.IntegerField("code")
    name = models.CharField(max_length=250)
    status = models.BooleanField(default=True)
    regions = models.ManyToManyField(Region,related_name='regions')

    def __str__(self):
        return self.name




