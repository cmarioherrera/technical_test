from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    # region

    path('region/',views.RegionList.as_view(),name='region_list'),
    path('region/new', views.RegionCreate.as_view(), name='region_new'),
    path('region/view/<int:pk>', views.RegionView.as_view(), name='region_view'),
    path('region/edit/<int:pk>', views.RegionUpdate.as_view(), name='region_edit'),
    path('region/delete/<int:pk>', views.RegionDelete.as_view(), name='region_delete'),

    path('township/',views.TownshipList.as_view(),name='township_list'),
    path('township/new', views.TownshipCreate.as_view(), name='township_new'),
    path('township/view/<int:pk>', views.TownshipView.as_view(), name='township_view'),
    path('township/edit/<int:pk>', views.TownshipUpdate.as_view(), name='township_edit'),
    path('township/delete/<int:pk>', views.TownshipDelete.as_view(), name='township_delete'),


]