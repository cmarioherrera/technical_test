# Forms
from django import forms

# Models
from .models import Region, Township

class RegionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegionForm, self).__init__(*args, **kwargs)
        self.fields['code'].widget.attrs = {
            'class': 'form-control input-lg'
        }
        self.fields['name'].widget.attrs = {
            'class': 'form-control'
        }
        

    class Meta:
        model = Region
        fields = ('code', 'name')
        labels = {
                'code': 'Codigo','name':'Nombre'
        }
        


class TownshipForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TownshipForm, self).__init__(*args, **kwargs)
        self.fields['code'].widget.attrs = {
            'class': 'form-control input-lg'
        }
        self.fields['name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['regions'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['status'].widget.attrs = {
            'class': 'form-control'
        }
        

    class Meta:
        model = Township
        fields = ('code', 'name','regions','status')
        labels = {
                'code': 'Codigo','name':'Nombre','regions':'Region','status':'Estado'
        }
