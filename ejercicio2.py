import string
import itertools

def main():
    numero = int(input("Número de columna:"))
    spanish_uppercase  = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,Ñ,O,P,Q,R,S,T,U,V,W,X,Y,Z'
    spanish_uppercase = spanish_uppercase.split(',')
    
    list_letter = []
    for i in range(1,4):
        for password in itertools.product(spanish_uppercase, repeat=i):
            list_letter.append((''.join(password)))

    print("Salida: {}".format(list_letter[numero-1]))
    print(list_letter)
if __name__ == "__main__":
    main()